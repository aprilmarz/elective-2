$(document).ready(function() {

	var CLEAR_TEXTFIELD = true
	var OPERATOR = ''
	var FIRST_NUM = 0

	$(".btn-num").click(function() {
		
		if( CLEAR_TEXTFIELD )
		{
			$("#textarea").val($(this).text())
			CLEAR_TEXTFIELD = false
		} else {
			$("#textarea").val($("#textarea").val() + $(this).text())
		}
		
	})


	$(".operation").click(function() {

		if( OPERATOR != '' )
		{
			calculate()

		}

		OPERATOR = $.trim($(this).text())
		FIRST_NUM = parseInt($("#textarea").val())
		CLEAR_TEXTFIELD = true

	})


	$("#button-equals").click(function() {
		calculate()
		CLEAR_TEXTFIELD = true
		OPERATOR = ''
		FIRST_NUM = parseInt($("#textarea").val())
	})

	$("#button-ce").click(function() {
		$("#textarea").val("0")
		CLEAR_TEXTFIELD = true
	})
	
	$("#button-c").click(function() {
		$("#textarea").val("0")
		CLEAR_TEXTFIELD = true
		OPERATOR = ''
		FIRST_NUM = 0
	})


	$("#button-back").click(function() {
		var t = $("#textarea").val()

		console.log(t)
		if( t.length != 1 && t != "0" )
		{
			$("#textarea").val(t.substring(0, t.length-1))
		} else if ( t.length == 1 && t != "0" ) {
			$("#textarea").val("0")
			CLEAR_TEXTFIELD = true
		}
	})

	function calculate ()
	{

		switch(OPERATOR)
		{
			case '+':
				$("#textarea").val(FIRST_NUM + parseInt($("#textarea").val()))
				break
			case '-':
				$("#textarea").val(FIRST_NUM - parseInt($("#textarea").val()))
				break
			case '*':
				$("#textarea").val(FIRST_NUM * parseInt($("#textarea").val()))
				break
			case '/':
				$("#textarea").val(FIRST_NUM / parseInt($("#textarea").val()))
				break
		}
	}

})